import React from 'react';
import './App.scss';

function App() {
  return (
    <div className="test--blue">
      This is just a test to check if this works.
    </div>
  );
}

export default App;
